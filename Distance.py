## Peter Garner MSc IT (PT) 02/04/2017

from GUIconnection import GUIconnection
from Pt import Pt

class Distance(GUIconnection):

    def displayName(self):
         return "Distance"

    def displayParameterName(self):
         # Use this to update the label next to the entry box
         return "Distance"

    ##################
    #  takes a newmeric value from the entry field along with a list of x / y coordinates.
    #  @pts numeric value from entry field
    #  @param list of x / coordinates
    #
    def thinPoints(self, pts, param):



        tupeCoordList = []
        num = 0
        for t in range(int(len(param) / 2)):  # convert the list into a list of x / y tuples
            coordTupe = (param[num], param[num + 1])
            tupeCoordList.append(coordTupe)
            num += 2
        count = 0
        firstCoordTupe = tupeCoordList[count] # get the first coord tuple
        fxCod = firstCoordTupe[0]
        fyCod = firstCoordTupe[1]
        first_point = Pt(fxCod, fyCod)  # create initial object
        newTupeList = []
        newTupeList.append(firstCoordTupe)  # add fist coord tupe to new tupe list
        nextCoordNum = 1
        while count < len(tupeCoordList)-1:
            myNextCodTup = tupeCoordList[nextCoordNum]  # get the next coord tuple
            nxCod = myNextCodTup[0]
            nyCod = myNextCodTup[1]
            next_point = Pt(nxCod, nyCod)  # (re)create next object

            euclDist = first_point.EuclideanDistance(next_point) # get the Euclidean Distance

            if euclDist <= float(pts):  # compare to user distance value
                nextCoordNum += 1  # if less or equal, increment the index of param to ge the next coord tuple, re-enter loop
                count += 1 # keep track of the location in param index
            else:
                first_point = Pt(nxCod, nyCod)  # if more that pts, create a new first_point object with same next_point coords
                newTupeList.append(myNextCodTup)  # add the coord set to the new list
                nextCoordNum += 1  # still need to increment the index of param
                count += 1 # keep track  of the location in param index and re enter loop
        nthList = []
        for n in newTupeList:  # return new list
            nthList.append(n[0])
            nthList.append(n[1])
        return nthList










