## Peter Garner MSc IT (PT) 02/04/2017

from tkinter import filedialog, messagebox, Frame, Canvas, Label, Listbox, StringVar, Entry, Button, Menu
import os.path
import importlib


class LineSimplification(Frame):

    ##################
    #  Contructs a UI for use with plugin files to simplify outline coordinates
    #  Missing pluging class files are listed to stdout
    #
    def __init__(self, master = None):
        Frame.__init__(self, master)
        self.grid()
        self.master.title("Line Simplification")
        self._scaled_X_Y = []
        newPointsList = []
        self.cwd = os.getcwd()
        self._loadDict = []

        self.CANV_HEIGHT = 600
        self.CANV_WIDTH = 430
        self._mCanvas = Canvas(bg='#E4E4E4', height=self.CANV_HEIGHT, width=self.CANV_WIDTH)
        self._mCanvas.grid(row=1, columnspan=5)
        Label(self, text= "select Method").grid(row = 0, column=0)
        self._listBox = Listbox(self, width=8, height=3)

        plugfile = open("plugins.txt", "r")
        pluglist = []
        for line in plugfile:
            line = line.strip("\n")
            if "-plugin file names-" in line:
                pass
            else:
                linePY = line + ".py"
                if os.path.isfile(linePY):  # check for no file,but adds to listbox if present
                    pluglist.append(line)
                else:
                    print("file for plugin " + '"' + line + '"' + " does not exist")
        plugfile.close()

        # dynamically create an instance and lookup listbox name
        self._instList = []
        plugnum = 0
        for p in pluglist:
            my_module = importlib.import_module(p)
            checkClass = getattr(my_module, p)
            instance = checkClass()
            self._listBox.insert(plugnum, instance.displayName())  # populate listbox using poly
            self._instList.append(instance) # create list of displayname objects
            plugnum += 1

        #place labels, boxes, buttons etc
        self._listBox.grid(row = 0, column = 1,padx=2, pady=2)
        self._listBox.bind('<<ListboxSelect>>', self.selectPlugin) # callback to selectPlugin
        self._valueType = Label(self, text="                 ").grid(row=0, column=2)
        self._valueEntry = StringVar()  # control variable for entry box
        Entry(self, width=10, textvariable = self._valueEntry).grid(row=0, column=3)
        Button(self, text="Process", command=self.drawmap).grid(row=0, column=5)

        # create a drop down to load / save csv files
        mbar = Menu(self)
        self.master.config(menu=mbar)
        filemenu = Menu(mbar, tearoff=0)
        mbar.add_cascade(label="File", menu=filemenu)
        filemenu.add_command(label = "Load", command = self.loadFile)
        filemenu.add_command(label = "Save", command = self.saveFile)

    ##################
    # callback method, takes displayParameterName return from objects
    # and uses form value entry box label
    # @listSelection list item passed to curselection
    #
    def selectPlugin(self, listSelection):
        if self._scaled_X_Y == []:  # ignores selection when no file loaded
            pass
        else:
            plugchoice = self._listBox.curselection()[0]
            self._valueType = self._instList[plugchoice].displayParameterName()
            Label(self, text=self._valueType).grid(row=0, column=2)

    ##################
    #  Loads CSv file of coordinates, scales the x / y to the canvas size and inverts the y, then draws the map
    #  Throws op ups if a file is already loaded or the loaded file type is not CSV
    #
    def loadFile(self):
        print("loading file...")
        if self._loadDict != []:
            self.throwPopUp("", "File already loaded")
            pass
        else:
            try:
                filename = filedialog.askopenfile(mode="r", initialdir=self.cwd)
                try:
                    if not filename.name.endswith('.csv'):
                        raise TypeError
                    else:
                        self._loadDict = {}     # faster to load into dictionary
                        count = 0
                        for line in filename:
                            lineList = line.split(",")
                            self._loadDict[int((lineList[0]))] = (float(lineList[1].strip()), float(lineList[2].strip())) # must be tuple value for dic
                            count += 1
                        filename.close()
                        rawCods = []
                        for j in range(1, len(self._loadDict)):
                            rawCods.append(self._loadDict[j][0])
                            rawCods.append(self._loadDict[j][1])

                        steplistx = rawCods[::2]
                        scaledListX = []  # list of converted values
                        mn = min(steplistx)
                        mx = max(steplistx)
                        for i in range(len(steplistx)):
                            scaledListX.append((steplistx[i] - mn) * (self.CANV_WIDTH / (mx - mn)))
                        # print(scaledListX)

                        # Convert values of x into range 0-400
                        steplisty = rawCods[1::2]
                        scaledListY = []  # list of converted values
                        mn = min(steplisty)
                        mx = max(steplisty)
                        # current range is -3 to 17
                        for i in range(len(steplisty)):
                            scaledListY.append((steplisty[i] - mn) * (self.CANV_HEIGHT / (mx - mn)))

                        # flip the Y coords to "un-mirror" the map
                        flip_scaledListY = []
                        for f in scaledListY:
                            flip_scaledListY.append(self.CANV_HEIGHT - f)

                        # create new scaled x / y list in positive range
                        self._scaled_X_Y = []
                        for m in range(int(len(rawCods) / 2)):
                            self._scaled_X_Y.append(scaledListX[m])
                            self._scaled_X_Y.append(flip_scaledListY[m])

                        #draw the 'load' map
                        self._mCanvas.create_polygon(self._scaled_X_Y, outline="black", fill="")
                except TypeError:
                    self.throwPopUp("", "Wrong file type; please load a CSV")
            except AttributeError:
                print("cancel loading file")

    ##################
    # Saves a file to the app directory, default extension is CSV
    # Throws pop up if no file is loaded
    #
    def saveFile(self):
        try:
            if self._loadDict == []:
                self.throwPopUp("", "File not loaded")
                pass
            else:
                newFile = filedialog.asksaveasfile(mode='w', defaultextension=".csv", initialdir=self.cwd)
                count = 1
                for i in self._newDict:
                    saveStr = str(count) + "," + str(self._newDict[i][0]) + "," + str(self._newDict[i][1]) + "\n"
                    newFile.write(saveStr)
                    count += 1
                newFile.close()
        except AttributeError:
            print("file not saved")


    ##################
    # Draws the map after the simplification type has been chose and a value entered.
    # Checks for both the above and will just ignore if one is missing.
    # Check the entry box value is numeric.
    # Creates a dictionary of the new values to save on file save time
    #
    def drawmap(self):
            if not self._scaled_X_Y or not self._valueType or self._valueEntry.get().strip() == '':
                self._valueEntry.set('')  ## clears field
                return
            try :
                entryVal = float(self._valueEntry.get())
            except ValueError:
                self.throwPopUp("Error", "Numeric value expected")
                return

            else:
                for d in self._instList:
                    if d.displayParameterName() == self._valueType:
                        newPointsList = d.thinPoints(entryVal, self._scaled_X_Y)
                        if newPointsList == []:
                            self._valueEntry.set('')
                            pass
                        else:
                            self._mCanvas.delete("all")
                            self._mCanvas.create_polygon(newPointsList, outline="black", fill="")
                        # create 'ready to save' new disctionary from newPointsList, remembering to (re)invert the Y coods
                            newFlipY = newPointsList[1::2]
                            new_FlipListY = []
                            for f in newFlipY:
                                new_FlipListY.append(self.CANV_HEIGHT - f)
                            XcodCount = 0
                            YcodCount = 0
                            dicCount = 1
                            self._newDict = {}
                            for t in range(int(len(newPointsList) / 2)):
                                codTupe = (newPointsList[XcodCount], new_FlipListY[YcodCount])
                                self._newDict[dicCount] = codTupe
                                XcodCount += 2
                                YcodCount += 1
                                dicCount += 1

    ##################
    #  Throws up a popup of type Warning
    #  @title popup box title
    #  @content popup content
    #
    def throwPopUp(self, title, content):
        return messagebox.showwarning(title, content)


if __name__ == "__main__":
    LineSimplification().mainloop()
