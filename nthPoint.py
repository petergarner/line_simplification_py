## Peter Garner MSc IT (PT) 02/04/2017

from GUIconnection import GUIconnection
from tkinter import messagebox


class nthPoint(GUIconnection):

    def displayName(self):
        # Use this to update the list box entry
        return "nthpoint"

    def displayParameterName(self):
        # Use this to update the label next to the entry box
        return "nthPoint"

    ##################
    #  takes a newmeric value from the entry field along with a list of x / y coordinates.
    #  Returns list of coordinates
    #
    #  @pts numeric value from entry field
    #  @param list of x / coordinates
    #
    def thinPoints(self, pts, param):
        if pts <= 0.5:
            messagebox.showwarning("", "nth point value, rounded to nearest integer, cannot be <= 0")
            return []
        ptsDec = pts % 1  #
        if ptsDec == 0:
            ptsTot = pts
        elif ptsDec <= 0.5:
            ptsTot = pts - ptsDec   #
        else:                       # round up / round down
            ptsTot = pts // 1 + 1   #

        tupeCoordList = []
        coordCount = 0
        for t in range(int(len(param) / 2)):
            coordTupe = (param[coordCount], param[coordCount + 1])
            tupeCoordList.append(coordTupe)
            coordCount += 2
        tupeCodListPts = tupeCoordList[::int(ptsTot)] # ptsTot is the step value
        nthList = []                                  # representing the nth point
        for n in tupeCodListPts:
            nthList.append(n[0])
            nthList.append(n[1])
        return nthList
